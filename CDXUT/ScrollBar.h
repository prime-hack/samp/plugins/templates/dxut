#ifndef SCROLLBAR_H
#define SCROLLBAR_H

#include "Control.h"
#include <algorithm>

#pragma pack( push, 1 )
struct CDXUTScrollBarData {
	enum ARROWSTATE { CLEAR, CLICKED_UP, CLICKED_DOWN, HELD_UP, HELD_DOWN };

	bool	   m_bShowThumb;
	DXUTRect   m_rcUpButton;
	DXUTRect   m_rcDownButton;
	DXUTRect   m_rcTrack;
	DXUTRect   m_rcThumb;
	int		   curentPos;
	int		   pagesize;
	int		   start;
	int		   end;
	int		   m_LastMouse[2];
	ARROWSTATE m_Arrow;
	double	   m_dArrowTS;

	inline int RectWidth( DXUTRect &rc ) { return ( ( rc ).right - ( rc ).left ); }
	inline int RectHeight( DXUTRect &rc ) { return ( ( rc ).bottom - ( rc ).top ); }

	void UpdateThumbRect();
	void ShowItem( int nIndex );
};
class CDXUTScrollBar : public CDXUTControl {
public:
	CDXUTScrollBarData scrollBarData;
};
#pragma pack( pop )


#endif // SCROLLBAR_H
