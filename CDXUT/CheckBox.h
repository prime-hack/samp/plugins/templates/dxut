#ifndef CHECKBOX_H
#define CHECKBOX_H

#include "Button.h"

#pragma pack( push, 1 )
class CDXUTCheckBox : public CDXUTButton {
public:
	virtual void SetCheckedInternal( bool bChecked, bool bFromInput = true ) = 0;

	bool m_bChecked;
	DXUTRect m_rcButton;
	DXUTRect m_rcText;

	bool GetChecked() const { return m_bChecked; }
	void SetChecked( bool bChecked ) { SetCheckedInternal( bChecked, false ); }
};
#pragma pack( pop )

#endif // CHECKBOX_H
