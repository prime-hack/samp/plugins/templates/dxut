#ifndef COMBOBOX_H
#define COMBOBOX_H

#include "Button.h"
#include "ScrollBar.h"

struct DXUTComboBoxItem {
	wchar_t strText[256];
	void *pData;
	DXUTRect rcActive;
	bool bVisible;
};

#pragma pack( push, 1 )
class CDXUTComboBox : public CDXUTButton {
public:
	int m_iSelected;
	int m_iFocused;
	int m_nDropHeight;
	struct {
		void *vtbl;
		CDXUTControlData m_Control;
		CDXUTScrollBarData m_ScrollBar;
	} m_ScrollBar;
	int m_nSBWidth;
	bool m_bOpened;
	DXUTRect m_rcText;
	DXUTRect m_rcButton;
	DXUTRect m_rcDropdown;
	DXUTRect m_rcDropdownText;
	char m_Items[6];
};
#pragma pack( pop )

#endif // COMBOBOX_H
