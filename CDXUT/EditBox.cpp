#include "EditBox.h"
#include "Misc.h"
#include <cstring>
#include <d3dx9core.h>
#include <stringapiset.h>
#include <usp10.h>
#if __has_include( <samp/SAMP/Memory.h>)
#	include <samp/SAMP/Memory.h>
#endif

inline int RectWidth( DXUTRect &rc ) {
	return ( ( rc ).right - ( rc ).left );
}
inline int RectHeight( DXUTRect &rc ) {
	return ( ( rc ).bottom - ( rc ).top );
}

int CUniBuffer::GetTextSize() {
	return wcslen( m_pwszBuffer );
}

bool CUniBuffer::SetBufferSize( int nNewSize ) {
	// If the current size is already the maximum allowed,
	// we can't possibly allocate more.
	if ( m_nBufferSize == 0xFFFF ) return false;

	int nAllocateSize = ( nNewSize == -1 || nNewSize < m_nBufferSize * 2 ) ? ( m_nBufferSize ? m_nBufferSize * 2 : 256 ) : nNewSize * 2;

	// Cap the buffer size at the maximum allowed.
	if ( nAllocateSize > 0xFFFF ) nAllocateSize = 0xFFFF;

#if __has_include( <samp/SAMP/Memory.h>)
	wchar_t *pTempBuffer = (wchar_t *)SAMP::Memory::Instance()->NewChars( sizeof( wchar_t ) * nAllocateSize );
#else
	wchar_t *pTempBuffer = new wchar_t[nAllocateSize];
#endif
	if ( !pTempBuffer ) return false;
	if ( m_pwszBuffer ) {
		std::memcpy( pTempBuffer, m_pwszBuffer, m_nBufferSize * sizeof( wchar_t ) );
#if __has_include( <samp/SAMP/Memory.h>)
		SAMP::Memory::Instance()->DeleteChars( m_pwszBuffer );
#else
		delete[] m_pwszBuffer;
#endif
	} else {
		std::memset( pTempBuffer, 0, sizeof( wchar_t ) * nAllocateSize );
	}

	m_pwszBuffer = pTempBuffer;
	m_nBufferSize = nAllocateSize;
	return true;
}

bool CUniBuffer::SetText( const wchar_t *wszText ) {

	int nRequired = int( wcslen( wszText ) + 1 );

	// Check for maximum length allowed
	if ( nRequired >= 0xFFFF ) return false;

	while ( m_nBufferSize < nRequired )
		if ( !SetBufferSize( -1 ) ) break;
	// Check again in case out of memory occurred inside while loop.
	if ( m_nBufferSize >= nRequired ) {
		wcsncpy( m_pwszBuffer, wszText, nRequired );
		m_bAnalyseRequired = true;
		return true;
	} else
		return false;
}

bool CUniBuffer::SetText( const char *lpString ) {
	if ( std::strlen( lpString ) > 1024 ) return false;

	wchar_t WideCharStr[2048]{ 0 };
	auto needLen = MultiByteToWideChar( 0, 0, lpString, std::strlen( lpString ), 0, 0 );
	if ( needLen < 2048 ) MultiByteToWideChar( 0, 0, lpString, std::strlen( lpString ), WideCharStr, needLen );

	return SetText( WideCharStr );
}

int CUniBuffer::Analyse() {
	if ( m_Analysis ) ScriptStringFree( (SCRIPT_STRING_ANALYSIS *)&m_Analysis );

	SCRIPT_CONTROL ScriptControl; // For uniscribe
	SCRIPT_STATE ScriptState; // For uniscribe
	ZeroMemory( &ScriptControl, sizeof( ScriptControl ) );
	ZeroMemory( &ScriptState, sizeof( ScriptState ) );
	ScriptApplyDigitSubstitution( NULL, &ScriptControl, &ScriptState );

	if ( !m_pFontNode ) return E_FAIL;

	HRESULT hr = ScriptStringAnalyse( m_pFontNode->pFont ? m_pFontNode->pFont->GetDC() : NULL,
									  m_pwszBuffer,
									  lstrlenW( m_pwszBuffer ) + 1, // NULL is also analyzed.
									  lstrlenW( m_pwszBuffer ) * 3 / 2 + 16,
									  DEFAULT_CHARSET,
									  SSA_BREAK | SSA_GLYPHS | SSA_FALLBACK | SSA_LINK,
									  0,
									  &ScriptControl,
									  &ScriptState,
									  NULL,
									  NULL,
									  NULL,
									  (SCRIPT_STRING_ANALYSIS *)&m_Analysis );
	if ( SUCCEEDED( hr ) ) m_bAnalyseRequired = false; // Analysis is up-to-date
	return hr;
}

int CUniBuffer::CPtoX( int nCP, bool bTrail, int *pX ) {
	*pX = 0; // Default

	HRESULT hr = S_OK;
	if ( m_bAnalyseRequired ) hr = Analyse();

	if ( SUCCEEDED( hr ) ) hr = ScriptStringCPtoX( (SCRIPT_STRING_ANALYSIS)m_Analysis, nCP, bTrail, pX );

	return hr;
}

int CUniBuffer::XtoCP( int nX, int *pCP, int *pnTrail ) {
	*pCP = 0;
	*pnTrail = FALSE; // Default

	HRESULT hr = S_OK;
	if ( m_bAnalyseRequired ) hr = Analyse();

	if ( SUCCEEDED( hr ) ) hr = ScriptStringXtoCP( (SCRIPT_STRING_ANALYSIS)m_Analysis, nX, pCP, pnTrail );

	// If the coordinate falls outside the text region, we
	// can get character positions that don't exist.  We must
	// filter them here and convert them to those that do exist.
	if ( *pCP == -1 && *pnTrail == TRUE ) {
		*pCP = 0;
		*pnTrail = FALSE;
	} else if ( *pCP > lstrlenW( m_pwszBuffer ) && *pnTrail == FALSE ) {
		*pCP = lstrlenW( m_pwszBuffer );
		*pnTrail = TRUE;
	}

	return hr;
}

void CDXUTEditBox::PlaceCaret( int nCP ) {
	m_nCaret = nCP;

	// Obtain the X offset of the character.
	int nX1st, nX, nX2;
	m_Buffer.CPtoX( m_nFirstVisible, FALSE, &nX1st ); // 1st visible char
	m_Buffer.CPtoX( nCP, FALSE, &nX ); // LEAD
	// If nCP is the NULL terminator, get the leading edge instead of trailing.
	if ( nCP == m_Buffer.GetTextSize() )
		nX2 = nX;
	else
		m_Buffer.CPtoX( nCP, TRUE, &nX2 ); // TRAIL

	// If the left edge of the char is smaller than the left edge of the 1st visible char,
	// we need to scroll left until this char is visible.
	if ( nX < nX1st ) {
		// Simply make the first visible character the char at the new caret position.
		m_nFirstVisible = nCP;
	} else
		// If the right of the character is bigger than the offset of the control's
		// right edge, we need to scroll right to this character.
		if ( nX2 > nX1st + RectWidth( m_rcText ) ) {
		// Compute the X of the new left-most pixel
		int nXNewLeft = nX2 - RectWidth( m_rcText );

		// Compute the char position of this character
		int nCPNew1st, nNewTrail;
		m_Buffer.XtoCP( nXNewLeft, &nCPNew1st, &nNewTrail );

		// If this coordinate is not on a character border,
		// start from the next character so that the caret
		// position does not fall outside the text rectangle.
		int nXNew1st;
		m_Buffer.CPtoX( nCPNew1st, FALSE, &nXNew1st );
		if ( nXNew1st < nXNewLeft ) ++nCPNew1st;

		m_nFirstVisible = nCPNew1st;
	}
}

void CDXUTEditBox::SetText( const wchar_t *wszText, bool bSelected ) {
	m_Buffer.SetText( wszText );
	m_nFirstVisible = 0;
	// Move the caret to the end of the text
	//	PlaceCaret( m_Buffer.GetTextSize() );
	m_nCaret = m_Buffer.GetTextSize();
	m_nSelStart = bSelected ? 0 : m_nCaret;
}

void CDXUTEditBox::SetText( const char *lpString, bool bSelected ) {
	m_Buffer.SetText( lpString );
	m_nFirstVisible = 0;
	// Move the caret to the end of the text
	//	PlaceCaret( m_Buffer.GetTextSize() );
	m_nCaret = m_Buffer.GetTextSize();
	m_nSelStart = bSelected ? 0 : m_nCaret;
}

const char *CDXUTEditBox::Text() {
	static char MultiByteStr[2048];
	std::memset( MultiByteStr, 0, 2048 );
	if ( !m_Buffer.m_pwszBuffer ) return "";
	auto needLen = WideCharToMultiByte( 0, 0, m_Buffer.m_pwszBuffer, wcslen( m_Buffer.m_pwszBuffer ), 0, 0, 0, 0 );
	if ( needLen <= 2048 ) {
		WideCharToMultiByte( 0, 0, m_Buffer.m_pwszBuffer, wcslen( m_Buffer.m_pwszBuffer ), MultiByteStr, needLen, 0, 0 );
		MultiByteStr[needLen] = 0;
	}
	return MultiByteStr;
}
