#ifndef EDITBOX_H
#define EDITBOX_H

#include "Control.h"

#pragma pack( push, 1 )
struct CUniBuffer {
	wchar_t *			 m_pwszBuffer;
	int					 m_nBufferSize;
	bool				 pad;
	struct DXUTFontNode *m_pFontNode;
	bool				 m_bAnalyseRequired;
	uint32_t			 m_Analysis;
	bool				 pad2;

	int	 GetTextSize();
	bool SetBufferSize( int nNewSize );
	bool SetText( const wchar_t *wszText );
	bool SetText( const char *lpString );
	int	 Analyse();
	int	 CPtoX( int nCP, bool bTrail, int *pX );
	int	 XtoCP( int nX, int *pCP, int *pnTrail );
};
class CDXUTEditBox : public CDXUTControl {
public:
	CUniBuffer m_Buffer;
	int		   m_nBorder;
	int		   m_nSpacing;
	DXUTRect   m_rcText;
	DXUTRect   m_rcRender[9];
	double	   m_dfBlink;
	double	   m_dfLastBlink;
	bool	   m_bCaretOn;
	int		   m_nCaret;
	bool	   m_bInsertMode;
	int		   m_nSelStart;
	int		   m_nFirstVisible;
	bool	   m_bSelected_;
	uint32_t   m_TextColor;
	uint32_t   m_SelTextColor;
	uint32_t   m_SelBkColor;
	uint32_t   m_CaretColor;
	bool	   m_bMouseDrag;
	char	   imeData[72];
	uint32_t   ImeWidth;
	DXUTRect   ImeRect;

	void		PlaceCaret( int nCP );
	void		SetText( const wchar_t *wszText, bool bSelected );
	void		SetText( const char *lpString, bool bSelected );
	const char *Text();
};
#pragma pack( pop )

#endif // EDITBOX_H
