#include "Dialog.h"

void CDXUTDialog::SendEvent( int nEvent, bool bTriggeredByUser, CDXUTControl *pControl ) {
	// If no callback has been registered there's nowhere to send the event to
	if ( m_pCallbackEvent == nullptr ) return;

	// Discard events triggered programatically if these types of events haven't been
	// enabled
	if ( !bTriggeredByUser && !m_bNonUserEvents ) return;

	m_pCallbackEvent( nEvent, pControl->controlData.m_ID, pControl, m_pCallbackEventUserContext );
}
