#include "ListBox.h"

void CDXUTListBox::SelectItem( int nNewIndex ) {
	// If no item exists, do nothing.
	if ( m_Items.m_nSize == 0 ) return;

	int nOldSelected = m_nSelected;

	// Adjust m_nSelected
	m_nSelected = nNewIndex;

	// Perform capping
	if ( m_nSelected < 0 ) m_nSelected = 0;
	if ( m_nSelected >= (int)m_Items.m_nSize ) m_nSelected = m_Items.m_nSize - 1;

	if ( nOldSelected != m_nSelected ) {
		if ( m_dwStyle & 1 ) { m_Items.m_pData[m_nSelected]->bSelected = true; }

		// Update selection start
		m_nSelStart = m_nSelected;

		// Adjust scroll bar
		m_ScrollBar.m_ScrollBar.ShowItem( m_nSelected );
	}

	controlData.m_pDialog->SendEvent( 0x702, true, this );
}

int CDXUTListBox::GetSelectedIndex( int nPreviousSelected ) {
	if ( nPreviousSelected < -1 ) return -1;

	if ( m_dwStyle & 1 ) {
		// Multiple selection enabled. Search for the next item with the selected flag.
		for ( int i = nPreviousSelected + 1; i < (int)m_Items.m_nSize; ++i ) {
			DXUTListBoxItem *pItem = m_Items.m_pData[i];
			if ( !pItem ) continue;

			if ( pItem->bSelected ) return i;
		}

		return -1;
	} else {
		// Single selection
		return m_nSelected;
	}
}
