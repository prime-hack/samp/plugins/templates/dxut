#ifndef BUTTON_H
#define BUTTON_H

#include "Static.h"

#pragma pack( push, 1 )
class CDXUTButton : public CDXUTStatic {
public:
	bool m_bPressed;
};
#pragma pack( pop )

#endif // BUTTON_H
