#ifndef MISC_H
#define MISC_H

#if __has_include( <render/color.h>)
#	include <render/color.h>
#elif __has_include( <base/d3d9/color.h>)
#	include <base/d3d9/color.h>
#endif
#include <cstdint>

enum DXUT_CONTROL_TYPE
{
	DXUT_CONTROL_BUTTON = 0x0,
	DXUT_CONTROL_STATIC = 0x1,
	DXUT_CONTROL_CHECKBOX = 0x2,
	DXUT_CONTROL_RADIOBUTTON = 0x3,
	DXUT_CONTROL_COMBOBOX = 0x4,
	DXUT_CONTROL_SLIDER = 0x5,
	DXUT_CONTROL_EDITBOX = 0x6,
	DXUT_CONTROL_IMEEDITBOX = 0x7,
	DXUT_CONTROL_LISTBOX = 0x8,
	DXUT_CONTROL_SCROLLBAR = 0x9,
};

enum DXUT_CONTROL_STATE
{
	DXUT_STATE_NORMAL = 0x0,
	DXUT_STATE_DISABLED = 0x1,
	DXUT_STATE_HIDDEN = 0x2,
	DXUT_STATE_FOCUS = 0x3,
	DXUT_STATE_MOUSEOVER = 0x4,
	DXUT_STATE_PRESSED = 0x5,
};

struct DXUTBlendColor {
#if __has_include( <render/color.h>) || __has_include( <base/d3d9/color.h>)
	SRColor States[6]; // ARGB
#else
	uint32_t States[6]; // ARGB
#endif
	float Current[4]; // RGBA
};

struct DXUTRect {
	uint32_t left;
	uint32_t top;
	uint32_t right;
	uint32_t bottom;
};

struct DXUTPoint {
	uint32_t x, y;
};

class CDXUTElement {
public:
	uint32_t iTexture;
	uint32_t iFont;
	uint32_t dwTextFormat;
	DXUTRect rcTexture;
	DXUTBlendColor TextureColor;
	DXUTBlendColor FontColor;
};

template<typename TYPE> class CGrowableArray {
public:
	TYPE *m_pData; // the actual array of data
	int m_nSize; // # of elements (upperBound - 1)
	int m_nMaxSize; // max allocated
};

struct DXUTTextureNode {
	char strFilename[260];
	class IDirect3DTexture9 *pTexture;
	uint32_t dwWidth;
	uint32_t dwHeight;
};

struct DXUTFontNode {
	char strFace[260];
	class ID3DXFont *pFont;
	uint32_t nHeight;
	uint32_t nWeight;
};

#endif // MISC_H
