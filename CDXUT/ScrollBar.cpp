#include "ScrollBar.h"

void CDXUTScrollBarData::UpdateThumbRect() {
	if ( end - start > pagesize ) {
		int nThumbHeight = std::max( RectHeight( m_rcTrack ) * pagesize / ( end - start ), 8 );
		int nMaxPosition = end - start - pagesize;
		m_rcThumb.top = m_rcTrack.top + ( curentPos - start ) * ( RectHeight( m_rcTrack ) - nThumbHeight ) / nMaxPosition;
		m_rcThumb.bottom = m_rcThumb.top + nThumbHeight;
		m_bShowThumb = true;

	} else {
		// No content to scroll
		m_rcThumb.bottom = m_rcThumb.top;
		m_bShowThumb = false;
	}
}

void CDXUTScrollBarData::ShowItem( int nIndex ) {
	if ( nIndex < 0 ) nIndex = 0;

	if ( nIndex >= end ) nIndex = end - 1;

	// Adjust position

	if ( curentPos > nIndex )
		curentPos = nIndex;
	else if ( curentPos + pagesize <= nIndex )
		curentPos = nIndex - pagesize + 1;

	UpdateThumbRect();
}
