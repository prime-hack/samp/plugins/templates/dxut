#ifndef CONTROL_H
#define CONTROL_H

#include "Misc.h"

class CDXUTDialog;

#pragma pack( push, 1 )
struct CDXUTControlData {
	bool m_bVisible;
	bool m_bMouseOver;
	bool m_bHasFocus;
	bool m_bIsDefault;
	int pos[2];
	int size[2];
	CDXUTDialog *m_pDialog;
	uint32_t m_Index;
	CGrowableArray<CDXUTElement *> m_Elements;
	int m_ID;
	DXUT_CONTROL_TYPE m_Type;
	uint32_t m_nHotkey;
	void *m_pUserData;
	bool m_bEnabled;
	DXUTRect m_rcBoundingBox;

	DXUT_CONTROL_TYPE GetType() const { return m_Type; }

	int GetID() const { return m_ID; }
	void SetID( int ID ) { m_ID = ID; }

	void SetHotkey( uint32_t nHotkey ) { m_nHotkey = nHotkey; }
	uint32_t GetHotkey() const { return m_nHotkey; }

	void SetUserData( void *pUserData ) { m_pUserData = pUserData; }
	void *GetUserData() const { return m_pUserData; }
};
class CDXUTControl {
public:
	virtual void _destruct( bool free = false ) = 0;
	virtual uint32_t OnInit() = 0;
	virtual void Refresh() = 0;
	virtual void Render( float fElapsedTime ) const = 0;
	virtual bool MsgProc( uint32_t uMsg, uint32_t wParam, uint32_t lParam ) = 0;
	virtual bool HandleKeyboard( uint32_t uMsg, uint32_t wParam, uint32_t lParam ) = 0;
	virtual bool HandleMouse( uint32_t uMsg, const DXUTPoint &pt, uint32_t wParam, uint32_t lParam ) = 0;
	virtual bool CanHaveFocus() const = 0;
	virtual void OnFocusIn() = 0;
	virtual void OnFocusOut() = 0;
	virtual void OnMouseEnter() = 0;
	virtual void OnMouseLeave() = 0;
	virtual void OnHotkey() = 0;
	virtual bool ContainsPoint( const DXUTPoint &pt ) const = 0;
	virtual void SetEnabled( bool bEnabled ) = 0;
	virtual bool GetEnabled() const = 0;
	virtual void SetVisible( bool bVisible ) = 0;
	virtual bool GetVisible() const = 0;
#if __has_include( <render/color.h>)
	virtual void SetTextColor( SRColor Color ) = 0;
#else
	virtual void SetTextColor( uint32_t Color ) = 0;
#endif
	virtual void UpdateRects() = 0;

	CDXUTControlData controlData;

	void SetLocation( int x, int y ) {
		controlData.pos[0] = x;
		controlData.pos[1] = y;
		UpdateRects();
	}
	void SetSize( int width, int height ) {
		controlData.size[0] = width;
		controlData.size[1] = height;
		UpdateRects();
	}
};
#pragma pack( pop )


#endif // CONTROL_H
