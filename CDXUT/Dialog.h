#ifndef DIALOG_H
#define DIALOG_H

#include "Control.h"

typedef void( __stdcall *PCALLBACKDXUTGUIEVENT )( uint32_t nEvent, int nControlID, CDXUTControl *pControl,
												  void *pUserContext );

#pragma pack( push, 1 )
struct DXUTElementHolder {
	uint32_t nControlType;
	uint32_t iElement;

	CDXUTElement Element;
};
class CDXUTDialog {
public:
	bool								m_bNonUserEvents;
	bool								m_bKeyboardInput;
	bool								m_bMouseInput;
	int									m_nDefaultControlID;
	double								m_fTimeLastRefresh;
	CDXUTControl *						m_pControlMouseOver;
	bool								m_bVisible;
	bool								m_bCaption;
	bool								m_bMinimized;
	bool								m_bDrag;
	char								m_wszCaption[255];
	int									m_x;
	int									m_y;
	int									m_width;
	int									m_height;
	int									m_nCaptionHeight;
	uint32_t							m_colorTopLeft;
	uint32_t							m_colorTopRight;
	uint32_t							m_colorBottomLeft;
	uint32_t							m_colorBottomRight;
	struct CDXUTDialogResourceManager * m_pManager;
	PCALLBACKDXUTGUIEVENT				m_pCallbackEvent;
	void *								m_pCallbackEventUserContext;
	CGrowableArray<int>					m_Textures;
	CGrowableArray<class ID3DXFont *>	m_Fonts;
	CGrowableArray<CDXUTControl *>		m_Controls;
	CGrowableArray<DXUTElementHolder *> m_DefaultElements;
	CDXUTElement						m_CapElement;
	struct CDXUTDialog *				m_pNextDialog;
	struct CDXUTDialog *				m_pPrevDialog;

	void SendEvent( int nEvent, bool bTriggeredByUser, CDXUTControl *pControl );
};
#pragma pack( pop )

#endif // DIALOG_H
