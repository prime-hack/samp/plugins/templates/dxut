#ifndef RESMAN_H
#define RESMAN_H

#include "Misc.h"

#pragma pack( push, 1 )
class CDXUTDialogResourceManager {
public:
	struct IDirect3DStateBlock9 *m_pStateBlock;
	class ID3DXSprite *m_pSprite;
	CGrowableArray<DXUTTextureNode *> m_TextureCache;
	CGrowableArray<DXUTFontNode *> m_FontCache;
	struct IDirect3DDevice9 *m_pd3d9Device;
};
#pragma pack( pop )

#endif // RESMAN_H
