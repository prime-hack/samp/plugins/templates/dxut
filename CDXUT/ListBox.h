#ifndef LISTBOX_H
#define LISTBOX_H

#include "Dialog.h"
#include "ScrollBar.h"

struct DXUTListBoxItem {
	wchar_t	 strText[256];
	void *	 pData;
	DXUTRect rcActive;
	bool	 bSelected;
};

#pragma pack( push, 1 )
class CDXUTListBox : public CDXUTControl {
public:
	DXUTRect m_rcText;
	struct {
		void *			   m_ControlVtbl;
		CDXUTControlData   m_Control;
		CDXUTScrollBarData m_ScrollBar;
	} m_ScrollBar;
	int								  unk[4];
	DXUTRect						  m_rcSelection;
	int								  m_nSBWidth;
	int								  m_nBorder;
	int								  m_nMargin;
	int								  m_nTextHeight;
	int								  m_dwStyle;
	int								  m_nSelected;
	int								  m_nSelStart;
	bool							  m_bDrag;
	CGrowableArray<DXUTListBoxItem *> m_Items;

	CDXUTScrollBar *GetScrollBar() const { return (CDXUTScrollBar *)&m_ScrollBar; }

	void SelectItem( int nNewIndex );
	int	 GetSelectedIndex( int nPreviousSelected = -1 );
};
#pragma pack( pop )

#endif // LISTBOX_H
