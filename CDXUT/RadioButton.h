#ifndef RADIOBUTTON_H
#define RADIOBUTTON_H

#include "CheckBox.h"

#pragma pack( push, 1 )
class CDXUTRadioButton : public CDXUTCheckBox {
public:
	virtual void SetCheckedInternal( bool bChecked, bool bClearGroup = true, bool bFromInput = false );
	uint32_t m_nButtonGroup;

	void SetChecked( bool bChecked, bool bClearGroup = true ) { SetCheckedInternal( bChecked, bClearGroup, false ); }
	void SetButtonGroup( unsigned int nButtonGroup ) { m_nButtonGroup = nButtonGroup; }
	unsigned int GetButtonGroup() const { return m_nButtonGroup; }
};
#pragma pack( pop )

#endif // RADIOBUTTON_H
